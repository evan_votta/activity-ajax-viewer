// Saves options to chrome.storage.sync.
function save_options() {
	var redditUsername = document.getElementById('redditUsername_field').value;
	var redditPassword = document.getElementById('redditPassword_field').value;
	var clientId = document.getElementById('clientId_field').value;
	var clientSecret = document.getElementById('clientSecret_field').value;
	var token = document.getElementById('token_field').value;
	
	chrome.storage.sync.set({
		redditUsername: redditUsername,
		redditPassword: redditPassword,
		clientId: clientId,
		clientSecret: clientSecret,
		token: token
	}, function() {
		// Update status to let user know options were saved.
		var status = document.getElementById('status');
		status.textContent = 'Options saved.';
		setTimeout(function() {
			status.textContent = '';
		}, 750);
	});
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
	// Use null values by default
	chrome.storage.sync.get({
		redditUsername: '',
		redditPassword: '',
		clientId: '',
		clientSecret: '',
		token: ''
	}, function(items) {
		document.getElementById('redditUsername_field').value = items.redditUsername;
		document.getElementById('redditPassword_field').value = items.redditPassword;
		document.getElementById('clientId_field').value = items.clientId;
		document.getElementById('clientSecret_field').value = items.clientSecret;
		document.getElementById('token_field').value = items.token;
	});
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click',save_options);
// Global vars
var linksData = [ ];
var token = "";
var username = "";
var numFails = 0;


/**
 * This function is used only to set the OAuth token
 */
function setToken(tokenValue) {
	token = tokenValue;
}


/**
 * This function is used to get a new OAuth token, based on the credentials
 * the user entered into the browser
 */
function authenticate() {
	// First get the credentials from the browser storage
	chrome.storage.sync.get({
		redditUsername: '',
		redditPassword: '',
		clientId: '',
		clientSecret: ''
	}, function(items) {
		if (items.redditUsername == null || items.redditUsername == '' || items.redditPassword == null || items.redditPassword == '' || items.clientId == null || items.clientId == '' || items.clientSecret == null || items.clientSecret == '') {
			console.error("Enter your credentials!!");
		}
		else {
			username = items.redditUsername;
			console.log("Trying to get new token for " + username);
			
			$.ajax({
				url: "https://www.reddit.com/api/v1/access_token",
				method: "POST",
				async: false,
				
				// Reddit application's clientID and clientPassword
				username: items.clientId,
				password: items.clientSecret,
				
				// User info, as we're using a user login as a grant action
				data: {
					"grant_type": "password",
					"username": items.redditUsername,
					"password": items.redditPassword
				},
				
				success: function(response) {
					setToken(response.access_token);
					console.log("Successfully got token: " + token);
					console.log(response);
					
					chrome.storage.sync.set({
						token: token
					}, function() {
						numFails++;
						if (numFails < 2) { refresh(); }
					});
				},
				
				error: function(e) {
					console.log("Could not retrieve token!");
					console.log(e);
				}
			});
		}
	});
}


/**
 * This function is called to synchronously add comments to their
 * respective parent links via the linksData global array.
 */
function assignCommentsToLinks(index,latestTimestamp,comments) {
	linksData[index].comments = comments;
	linksData[index].latestTimestamp = latestTimestamp;
}


/**
 * This function serves as a parent function to others, and retreives the entire
 * user's history of links and comments.
 */
function getLinks() {
	
	if (token == null || token == "") {
		authenticate();
	}
	
	// First, get the last three relevant activities
	var promise = $.ajax({
		url: "https://oauth.reddit.com/user/" + username + "/submitted",
		async: false,
		headers: {
			"Authorization": "bearer " + token,
			//,"User-Agent": "ActivityAjaxView/0.1 by YourUsername"
		},
		
		error: function(e) {
			console.error("ERROR: getLinks()");
			console.error(e);
			
			// We probably have an expired OAuth token, get a new one
		},
		
		success: function(response) {
			// Get the links
			
			// Loop through the links, storing their essential data
			if (response != null && response.data != null && response.data.children != null) {
				for (var i = 0; i < response.data.children.length; i++) {
					linksData[i] = {
						"url": response.data.children[i].data.url,
						"name": response.data.children[i].data.name,
						"created_utc": response.data.children[i].data.created_utc,
						"num_comments": response.data.children[i].data.num_comments,
						"title": response.data.children[i].data.title,
						"latestTimestamp": response.data.children[i].data.created_utc
					};
					
					// For the links that have comments, get them so we can sort by order
					if (parseInt(linksData[i].num_comments,10) > 0) {
						getCommentsByLinkUrl(i,linksData[i].url);
					}
				}
			}
		}
	});
}


/**
 * This function gets a list of comments on a given link.
 */
function getCommentsByLinkUrl(index,url) {
	
	var comments = [ ];
	var numComments = 0;
	var latestTimestamp;
	
	var promise = $.ajax({
		url: url + ".json",
		async: false,
		
		success: function(response) {
			// Get all comments
			// console.log(response);
			for (var i = 0; i < response.length; i++) {
				for (var j = 0; j < response[i].data.children.length; j++) {
					if (response[i].data.children[j].kind.toLowerCase() == 't1') {
						// We have a comment
						// console.log("--> id: " + response[i].data.children[j].data.id, + " created_utc: " + response[i].data.children[j].data.created_utc.toString());
						comments[numComments++] = {
							"parent_id" : response[i].data.children[j].data.parent_id,
							"id": response[i].data.children[j].data.id,
							"created_utc": response[i].data.children[j].data.created_utc,
						};
						
						if (latestTimestamp == null || latestTimestamp < response[i].data.children[j].data.created_utc) {
							latestTimestamp = response[i].data.children[j].data.created_utc;
						}
					}
				}
			}
			assignCommentsToLinks(index,latestTimestamp,comments);
		}
	});
}


/**
 * This function refreshes the view.
 */
function refresh() {
	// All of this is done asynchronously
	console.log("Refreshing the Activity AJAX Viewer...");
	
	getLinks();
	
	if (linksData.length > 0) {
		// Sort the array in timestamp order
		linksData.sort(function(a, b) {
			var keyA = a.latestTimestamp,
				keyB = b.latestTimestamp;
			// Compare the 2 dates
			if (keyA > keyB) return -1;
			if (keyA < keyB) return 1;
			return 0;
		});
		
		// Just return the top three
		var rowsContainer = $("#activityAjaxViewer_rows").empty();
		
		// Cycle through the links
		for (var i = 0; i < linksData.length; i++) {
			// The 0 there is the key, which sets the date to the epoch
			var d = new Date(0);
			d.setUTCSeconds(linksData[i].latestTimestamp);
			
			var row = '' + 
				'<div class="reddit-link even first-half thing id-' + linksData[i].name + '">' + 
				
				'	<div class="reddit-entry entry">' +
				'		<a class="reddit-link-title may-blank" href="' + linksData[i].url + '" rel="nofollow">' + linksData[i].title + '</a>' +
				'		<br>' + 
				'		<small>' + 
				'			<span class="word">Updated: ' + d.toLocaleDateString() + ", " + d.toLocaleTimeString() + '</span>' +
				'		</small>' + 
				'	</div>' + 
				'	<div class="reddit-link-end"></div>' + 
				'</div>';
			$(rowsContainer).append(row);
			
			// We only want to return a max number of three links
			if (i == 2) { break; }
		}
	}
}


/**
 * This is the main function that places the container
 * on the page. It is run when the DOM loads.
 */
$(document).ready(function() {
	
	chrome.storage.sync.get({
		redditUsername: '',
		redditPassword: '',
		clientId: '',
		clientSecret: '',
		token: ''
	}, function(items) {
		username = items.redditUsername;
		token = items.token;
		
		if (items.redditUsername == null || items.redditUsername == '' || items.redditPassword == null || items.redditPassword == '' || items.clientId == null || items.clientId == '' || items.clientSecret == null || items.clientSecret == '') {
			console.error("Enter your credentials!!");
			throw new Error("Enter your credentials!");
		}
		else {
			console.log("Got username: " + username);
		
			// Create the HTML object
			var view ='' + 
				'<div id="activityAjaxViewer" class="spacer">' + 
				'	<div class="sidecontentbox">' + 
				'		<div class="title">' + 
				'			<h1>Activity AJAX Viewer</h1>' + 
				'		</div>' + 
				'		' + 
				'		<ul class="content">' + 
				'			<li>' + 
				'				<div class="gadget">' + 
				'					<div class="click-gadget">' + 
				'						<div id="activityAjaxViewer_rows">' + 
											// Rows go here
				'						</div>' + 
				'					</div>' + 
				'				</div>' + 
				'			</li>' + 
				'		</ul>' + 
				'	</div>' + 
				'</div>';
				
			// Insert into DOM
			var sidePanel = $(".side");
			if (sidePanel.length == 1) {
				$(sidePanel[0]).append(view);
				refresh();
				
				// Re-call this every 30 seconds
				setInterval(function() {
					refresh();
				}, 1000 * 30);
			}
		}
	});
});
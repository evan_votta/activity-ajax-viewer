# Activity AJAX Viewer #

This README explains what this project is and how it works.

## Summary ##

#### Quick summary: ####
This project works as a Google Chrome browser extension that runs in the browser and inserts a small little UI module when visiting
Reddit pages. The element displays a list of the three most-recently updated links a user is affiliated with. At the moment, the only
"updates" that are being considered are the creation of the link, and a comment added to it. Voting, deleting, etc, are not yet built-in.

#### How do I get set up? ####

1)	First, log into Reddit and create an app. Follow the directions here (https://www.reddit.com/prefs/apps/) to do that. Make sure to note the name of the application.
2)	Install the Activity AJAX Viewer extension in your Google Chrome browser. Once you get that set up, make sure to go to chrome://extensions/ and click the Options button on the extension. Enter in your Reddit username and password, as this is using password grant type authentication. Then also add in your new app's Client ID and Secret (from step 1). You do not need to enter data into the Token field, it will be filled in automatically later, but you can manually override here if you want later.
3)	Navigate to a main page where the Recently Viewed Links would normally show up on the right side of the screen. This includes the home page, subreddits, and links. It does not include account settings pages, etc.